--CREATE DATABASE Bernard
--GO

--USE Bernard
--GO

DROP TABLE [Set]

CREATE TABLE [Set]
(
[Id] INT NOT NULL IDENTITY(1, 1), -- Identity (intialiser, increment)
[Name] NVARCHAR(255) NOT NULL -- NVARCHAR(max char), type (255 max characters long)
)
GO

CREATE TABLE [Card]
(
[Id] INT NOT NULL IDENTITY(1, 1),
[Name] NVARCHAR(255) NOT NULL,
[SetId] INT NOT NULL,
CONSTRAINT [FK_Card_SetId] FOREIGN KEY ([SetId]) REFERENCES [Set]([Id]) -- Gives a ton of extra information into this column. Foreign key means that the column references something in another table
) -- FOREIGN KEY references table [Set]. Advantage of this is if you add a card that has a set id that doesn't exist this will throw a hissy fit.
GO

INSERT INTO [Set]
(
Name
) 
VALUES
('WhoreWin')
GO

INSERT INTO [Set]
(
Name
) 
VALUES
('WhoreSplash')
GO

INSERT INTO [Card]
(	Name, SetId) 
VALUES
(	'Lol Ring', 1),
(	'Wankbuns', 1)

SELECT *from[card]

INSERT INTO [Card]
(
Name,
SetId
) 
VALUES
(	'Lol Ring', (SELECT [Id] FROM [Set] Where Name = 'WhoreWin')) -- 1 colourless, taps for 2.5 mana
--

SELECT 
c.[Id] AS 'Card Id',
c.[Name] AS 'Card Name',
s.[Name] AS 'Set Name'
FROM [Card] c
INNER JOIN [Set] s ON c.SetId = s.Id

/*
a	b	LEFT, RIGHT, INNER
a	b	LEFT, RIGHT, INNER
a	LEFT
b	RIGHT
*/

select * from [set]

UPDATE [Set]
SET [Name]= 'Arse' 
WHERE [Id] = 1

select * from [set] where name like '%se%'